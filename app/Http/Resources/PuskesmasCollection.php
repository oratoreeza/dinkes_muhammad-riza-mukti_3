<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PuskesmasCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => 'success',
            'code' => 200, // Saya pribadi menghindari response code brada di body, karna hal ini menjadi redundant dengan response code yg ada di header
            'count' => $this->count(),
            'data' => $this->collection($this['data'])
        ];
    }
}
