<?php

namespace App\Http\Controllers;

use App\Http\Resources\PuskesmasCollection;
use App\Services\DinkesApiService;

class PuskesmasController extends Controller
{

    public function all(DinkesApiService $dinkesApiService)
    {
        // Menggunakan service sebagai abstraksi dari API dinkes. Untuk memudahkan programmer 
        // dalam memahami, memelihara, dan menggunakan Endpoint yang disediakan oleh API Dinkes.
        // File service dapat diakses di folder app/services/DinkesApiService.php

        // Menggunakan fitur collection Laravel untuk mempermudah proses mapping dan filter array
        $puskesCollection = collect($dinkesApiService->getPuskesmas());
        $wilayahCollection = collect($dinkesApiService->getWilayah());

        $puskesWithWilayah = collect($puskesCollection)->map(function ($puskesmas) use ($wilayahCollection) {
            // Mencari object wilayah yang memiliki id kecamatan sama dengan yang ada di object puskesmas
            $wilayah = $wilayahCollection->firstWhere("id_kecamatan", $puskesmas['kode_kecamatan']);

            // Assignment kode object dan nama object wilayah (provinsi,kota,kecamatan) kepada object puskesmas
            $puskesmas['provinsi'] = ['id_provinsi' => $wilayah['id_provinsi'], 'nama_provinsi' => $wilayah['nama_provinsi']];
            $puskesmas['kota'] = ['id_kota' => $wilayah['id_kota'], 'nama_kota' => $wilayah['nama_kota']];
            $puskesmas['kecamatan'] = ['id_kecamatan' => $wilayah['id_kecamatan'], 'nama_kecamatan' => $wilayah['nama_kecamatan']];
            
            return $puskesmas;
        });

        // Mapping data dan metadata dari response yang ingin ditampilkan
        // Menggunakan fitur response() Laravel untuk konversi array/object kedalam json
        return response()->json([
            'status' => 'success',
            'code' => 200, // Saya pribadi menghindari response code di body, karna hal ini menjadi redundant dengan response code yg ada di header
            'count' => $puskesWithWilayah->count(),
            'data' => $puskesWithWilayah,
        ]);

        // Untuk return yang lebih kompleks seperti halnya pagination dan object relationship
        // saya menggunakan fitur ResourceCollection Laravel sebagaimana berikut:

        // return new PuskesmasCollection($puskesWithWilayah);
    }
}
